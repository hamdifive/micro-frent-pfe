import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-confirm-mail',
  templateUrl: './confirm-mail.component.html',
  styleUrls: ['./confirm-mail.component.scss']
})
export class ConfirmMailComponent implements OnInit {
expired=false;
  constructor(private route: ActivatedRoute,private deliverymanService:DeliveryManService,private location: Location) { }

  ngOnInit() {
    this.getToken()
     this.location.replaceState(Location.joinWithSlash('confirmation',''))
  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
  getToken(){
    this.readUrlParams((routeParams, queryParams) => {
      let token = routeParams.token;
      let mail = routeParams.mail;
      this.deliverymanService.confirmDeliveryManMail(token,mail).subscribe(data=>{
        if(!data){this.expired=true;}
      })
  
    });
  }
}
