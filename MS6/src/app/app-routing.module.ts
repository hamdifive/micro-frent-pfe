import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import {DeliveryManProfilComponent} from'./delivery-man-profil/delivery-man-profil.component'
import {ConfirmMailComponent} from'./confirm-mail/confirm-mail.component'
import {DeliveryManSigninComponent} from'./delivery-man-signin/delivery-man-signin.component'






const routes: Routes = [
  { path: 'MS6', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: DeliveryManProfilComponent
        
    },
    {
      path: 'exemple2',
      component: ConfirmMailComponent
      
  },
  {
    path: 'exemple3',
    component: DeliveryManSigninComponent
    
}
  
  
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
