import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryManSigninComponent } from './delivery-man-signin.component';

describe('DeliveryManSigninComponent', () => {
  let component: DeliveryManSigninComponent;
  let fixture: ComponentFixture<DeliveryManSigninComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryManSigninComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryManSigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
