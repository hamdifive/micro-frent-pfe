import { Component, OnInit,ViewChild, HostListener } from '@angular/core';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
import { passwordValidator } from 'src/app/_models/custom-validators';
import { GoogleService } from 'src/app/_services/google.service';
import { AuthService} from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
@Component({
  selector: 'app-delivery-man-signin',
  templateUrl: './delivery-man-signin.component.html',
  styleUrls: ['./delivery-man-signin.component.scss']
})
export class DeliveryManSigninComponent implements OnInit {
  @ViewChild('basicModal', { static: true }) basicModal: any;

  public innerWidth: any;
    /*
  0 : no error
  1:signin mail or password invalid
  2:signup mail exists in database
  3:signup mail invalid
  4:signin account not verified
  5:signup error
  6:signin success
  */
 message=0;
  signInForm: FormGroup;
  signUpForm: FormGroup;
  new:boolean = false;
  selectedFiles: FileList;
  delievryman:DeliveryMan= new DeliveryMan();
  checkbox=false;
  phonenumber="";
  constructor(private deliveryFeeService:DeliveryFeeService, private authService: AuthService,private googleService:GoogleService,private deliverManService:DeliveryManService,private loginService:DeliveryManLoginService) { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.signInFormBuild();
    this.signUpFormBuild();

   
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  ngAfterViewChecked(){
    

  }
 
  signInFormBuild(){
    this.signInForm =  new FormGroup({
      email: new FormControl('',[ Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
  }
  signUpFormBuild(){
    this.signUpForm =  new FormGroup({
      firstName:new FormControl('',Validators.required),
      lastName:new FormControl('',Validators.required),
      email: new FormControl('', [ Validators.email, Validators.required]),
      address: new FormControl('', Validators.required),
 /*      cin: new FormControl('', [Validators.required,Validators.pattern("[0-9]{8,8}$")]),
      cinImage: new FormControl('', [Validators.required]), */
      phoneNumber: new FormControl('', [Validators.required,Validators.pattern("[0-9]{8,8}$")]),
      password: new FormControl('', [Validators.required,Validators.minLength(5),passwordValidator()])
    });

  }
  get getSignInFormEmail() {
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }
  get getSignupFormName() {
    return this.signUpForm.get('firstName');
  }

  get getSignupFormEmail() {
    return this.signUpForm.get('email');
  }
  get getSignUpFormCin() {
    return this.signUpForm.get('cin');
  }
  get getSignUpFormCinImage() {
    return this.signUpForm.get('cinImage');
  }
  get getSignUpFormTel() {
    return this.signUpForm.get('phoneNumber');
  }

  get getSignupFormPassword() {
    return this.signUpForm.get('password');
  }

  get getSignupFormLastName() {
    return this.signUpForm.get('lastName');
  }
  get getSignupFormAddress() {
    return this.signUpForm.get('address');
  }
  
/*   selectFile(event) {
    this.selectedFiles = event.target.files;
    this.signUpForm.get('cinImage').setValue(this.selectedFiles.item(0).name) 
  } */
  signin(){

      let deliverMan= new DeliveryMan(this.signInForm.value);
      this.deliverManService.getLoginDeliveryMan(deliverMan.email,deliverMan.password).subscribe(newdeliverMan=>{
        if(newdeliverMan){     
          this.message=0;   
          (newdeliverMan.verified)?this.update(newdeliverMan,true):this.message=4;
        }else {
          this.message=1;
        }
        
      });
  }
  update(deliveryman:DeliveryMan,notsocial?){
    deliveryman.isConnected=true;
    if ('geolocation' in navigator) {

      navigator.geolocation.getCurrentPosition((position) => {
        deliveryman.coordinates={latitude:position.coords.latitude,longitude:position.coords.longitude}
        
            this.deliverManService.updateDeliveryManLocation(deliveryman).subscribe(newdeliverMan=>{
          this.loginService.login(newdeliverMan._id,notsocial);
        });
        this.deliverManService.updateDeliveryMan(deliveryman).subscribe(newdeliverMan=>{
          this.delievryman=newdeliverMan
        });
      },(error)=> {
        
        this.googleService.getLocation().subscribe(coords=>{
          deliveryman.coordinates={latitude:coords.location.lat,longitude:coords.location.lng}
            this.deliverManService.updateDeliveryManLocation(deliveryman).subscribe(newdeliverMan=>{
          this.loginService.login(newdeliverMan._id,notsocial);})
          this.deliverManService.updateDeliveryMan(deliveryman).subscribe(newdeliverMan=>{
            this.delievryman=newdeliverMan
          });
          
          
        }) 
      });
    }
          }
      check(){
        if(this.getSignupFormEmail.valid){
        this.deliveryFeeService.getOneDeliveryFee().subscribe(data=>{
          let value = data as any
          this.deliverManService.checkMail(value.key,this.signUpForm.value.email).subscribe( data =>{
            if(data.status===200){
              this.message=0;
            }
            else {
              this.message=3
            }
          })
        })
      }
      }
/*   signup(){
    let deliverMan= new DeliveryMan(this.signUpForm.value);
    this.deliverManService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
      deliverMan.cinImage = data 
      deliverMan.active=false;
      this.deliverManService.createDeliveryMan(deliverMan).subscribe(newdeliverMan=>{
        this.message=0
        if(newdeliverMan===true as any ){
          this.message=2;
        }else if(newdeliverMan!=null) {
          this.message=6;
          this.new=false;
          
          this.signUpFormBuild();
          
        }
  
      }  );
    },err=>{this.message=5; } )


  } */
  signup(){
    let deliverMan= new DeliveryMan(this.signUpForm.value);
    deliverMan.active=false;
    this.deliverManService.createDeliveryMan(deliverMan).subscribe(newdeliverMan=>{
      this.message=0
      if(newdeliverMan===true as any ){
        this.message=2;
      }else if(newdeliverMan!=null) {
        this.message=6;
        this.new=false;
        
        this.signUpFormBuild();
        
      }

    }  );


  }
  signupSocial(user:SocialUser){
   let deliveryman = new DeliveryMan({'firstName':user.firstName,'lastName':user.lastName,'email':user.email,'password':user.id,'verified':true,'active':false});
    this.delievryman=deliveryman;
    this.basicModal.show()
  
  }
  save(){
    this.delievryman.phoneNumber=this.phonenumber;
    this.deliverManService.createDeliveryManSocial(this.delievryman).subscribe(newdeliverMan=>{
      if(newdeliverMan===true as any ){
        this.message=2;
      }else if(newdeliverMan!=null) {
        this.message=0;
        this.deliverManService.confirmDeliveryManMailSocial(newdeliverMan._id).subscribe(data=>console.log(data))
        this.loginService.login(newdeliverMan._id,false)
        
      }
     })
    
     
     }
  signinSocial(user:SocialUser){
    let deliveryman = new DeliveryMan({'email':user.email,'password':user.id});
    this.deliverManService.getLoginDeliveryMan(deliveryman.email,deliveryman.password).subscribe(newdeliverMan=>{
      if(newdeliverMan){     
        this.message=0;   
        (newdeliverMan.verified)?this.update(newdeliverMan,false):this.message=4;

      }else {
        this.message=1;
      }

    })
    this.deliverManService.updateDeliveryMan(deliveryman).subscribe(newdeliverMan=>{
      newdeliverMan.isConnected==true})

   
   }
  signWithSocial(social,action): void {
   
    let provider = (social==='FB')?FacebookLoginProvider.PROVIDER_ID:GoogleLoginProvider.PROVIDER_ID; 
    this.authService.signIn(provider).then(user => {
      (action==='IN')?this.signinSocial(user):this.signupSocial(user)
    });
    
  }


}
