import { Component, OnInit } from '@angular/core';
import { DeliveryManLoginService } from 'src/_services/deliveryMan/delivery-login.service';
import { Router } from '@angular/router';
import { DeliveryManService } from 'src/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/_models/deliveryMan';

@Component({
  selector: 'delivery-man-header-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit { 

  active:Boolean;
  delievryman:DeliveryMan= new DeliveryMan();
  constructor(public router: Router,private loginService:DeliveryManLoginService,private deliverymanService:DeliveryManService) { }

  ngOnInit() {
    this.deliverymanService.getDeliveryMan(this.loginService.getDeliveryMan()).subscribe(data=>{
      this.active=data.active
    })
  }
  logout(){
   
    this.deliverymanService.getDeliveryMan(this.loginService.getDeliveryMan()).subscribe(delievryman=>{
    
    delievryman.isConnected=false;
  
    this.deliverymanService.updateDeliveryMan(delievryman).subscribe(DeliveryMan=>{
      this.loginService.logout();
    });  
    });
   
  }
}
