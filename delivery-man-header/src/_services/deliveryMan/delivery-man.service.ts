import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  DeliveryMan } from 'src/_models/deliveryMan';
@Injectable({
  providedIn: 'root'
})
export class DeliveryManService {

  env : Env = new Env();
  path=location.origin+"/deliveryman/confirmation"
  private Url = this.env.host+'/deliverymanservice/';
  constructor(private http: HttpClient) { }
       /** GET: checkMail */
       checkMail(key,mail): Observable<any> {
      
        return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
        .pipe(
          catchError(err => {
            console.log('caught mapping error and rethrowing', err);
            return throwError(err );
        })
        )
     
  
   }
  
     
       
           /** PUT: update DeliveryMan   */
        updateDeliveryMan (deliveryMan: DeliveryMan): Observable<DeliveryMan> {
            return this.http.put<DeliveryMan>(this.Url+'DeliveryMan/'+deliveryMan._id, deliveryMan)
              .pipe(
                catchError(this.handleError<DeliveryMan>('updateDeliveryMan', deliveryMan))
              );
          }
                     /** PUT: update DeliveryMan'location   */
      
             /** GET: get one DeliveryMan   */
        getDeliveryMan (id: string): Observable<DeliveryMan> {
          return this.http.get<DeliveryMan>(this.Url+'DeliveryMan/'+id)
            .pipe(
              catchError(this.handleError<DeliveryMan>('getDeliveryMan', null))
            );
        }
   
        /** GET: Login  */
    
             /** DELETE: delete one DeliveryMan   */
 
        private handleError<T> (operation = 'operation', result?: T) {
          return (error: any): Observable<T> => {
       
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
       
            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);
       
            // Let the app keep running by returning an empty result.
            return of(result as T);
          };
        }
}
