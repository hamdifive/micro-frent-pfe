import { Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Recipient } from 'src/app/_models/recipient';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { RecipientLoginService } from 'src/app/_services/recipient/recipient-login.service';
import { passwordValidator } from 'src/app/_models/custom-validators';
import { AuthService} from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-recipient-signin',
  templateUrl: './recipient-signin.component.html',
  styleUrls: ['./recipient-signin.component.scss']
})

export class RecipientSigninComponent implements OnInit {
  @ViewChild('basicModal', { static: true }) basicModal: any;
      /*
  0 : no error
  1:signin mail or password invalid
  2:signup mail exists in database
  3:signup mail invalid
  4:signin account not verified
  5:signup error
  6:signin success
  */
 message=0;
  signInForm: FormGroup;
  signUpForm: FormGroup;
  new:boolean = false;
  checkbox=false;
  recipient:Recipient=new Recipient();
  phonenumber="";
  constructor(private deliveryFeeService:DeliveryFeeService,private authService: AuthService, private recipientService:RecipientService, private loginService:RecipientLoginService) { }

  ngOnInit() {
    this.signInFormBuild();
    this.signUpFormBuild();
  }


  signInFormBuild(){
    this.signInForm =  new FormGroup({
      email: new FormControl('',[ Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
  }

  signUpFormBuild(){
    this.signUpForm =  new FormGroup({
      firstName:new FormControl('',Validators.required),
      lastName:new FormControl('',Validators.required),
      phoneNumber: new FormControl('', [Validators.required,Validators.pattern("[0-9]{8,8}$")]),
     // cin: new FormControl('', [Validators.required,Validators.pattern("[0-9]{8,8}$")]),
      address: new FormControl('', Validators.required),
      email: new FormControl('', [ Validators.email, Validators.required]),
      password: new FormControl('', [Validators.required,Validators.minLength(5),passwordValidator()]),
    });
  }

  get getSignInFormEmail() {
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }

  get getSignupFormFName() {
    return this.signUpForm.get('firstName');
  }

  get getSignupFormLName() {
    return this.signUpForm.get('lastName');
  }

get getSignupFormPhone() {
  return this.signUpForm.get('phoneNumber');
}
get getSignupFormNcin() {
  return this.signUpForm.get('cin');
}
get getSignupFormAdresse () {
  return this.signUpForm.get('address');
} 
get getSignupFormEmail() {
  return this.signUpForm.get('email');
}
get getSignupFormPassword() {
  return this.signUpForm.get('password');
}

signin(){
let recipient= new Recipient(this.signInForm.value);
this.recipientService.getLoginRecipient(recipient.email,recipient.password).subscribe(recipient=>{
  if(recipient){
    this.message=0;   
    (recipient.verified)? this.loginService.login(recipient._id,true):this.message=4;
 
   
  }else {
    this.message=1;
  }
 
})

}


 check(){
   if(this.getSignupFormEmail.valid){
  this.deliveryFeeService.getOneDeliveryFee().subscribe(data=>{
    let value = data as any
    this.recipientService.checkMail(value.key,this.signUpForm.value.email).subscribe( data =>{
      if(data.status===200){
        this.message=0;
      }
      else {
        this.message=3
      }
    })
  })}

}
signup(){
  let recipient= new Recipient(this.signUpForm.value);
  recipient.type="signup";
  this.recipientService.createRecipient(recipient).subscribe(newrecipient=>{
    if(newrecipient ===true as any ){
      this.message=2;
    }else{ if(newrecipient!=null) {
      console.log('tt')
      this.message=6;
      this.signUpFormBuild(); 
      
    }}
   
  })

}

signinSocial(user:SocialUser){
  let recipient = new Recipient({'email':user.email,'password':user.id});
  this.recipientService.getLoginRecipient(recipient.email,recipient.password).subscribe(newRecipient=>{
    if(newRecipient){        
      this.message=0;   
      this.loginService.login(newRecipient._id,false)
  
    }else {
      this.message=1;
    }
   
  
  })
 
 }
 signupSocial(user:SocialUser,type){
  let recipient = new Recipient({'firstName':user.firstName,'lastName':user.lastName,'email':user.email,'password':user.id,'verified':true,'type':type });
  this.recipient=recipient;
   this.basicModal.show()
 
 }
 save(){
this.recipient.phoneNumber=this.phonenumber;
this.recipientService.createRecipientSocial(this.recipient).subscribe(newrecipient=>{
  if(newrecipient===true as any ){
   this.message=2;
  }else if(newrecipient!=null) {
   this.message=0;
   this.recipient=newrecipient;
   this.loginService.login(newrecipient._id,false)
    
  }
 })

 
 }
signWithSocial(social,action): void {
 
  let provider = (social==='FB')?FacebookLoginProvider.PROVIDER_ID:GoogleLoginProvider.PROVIDER_ID; 
  let type = (social==='FB')?"facebook":"google"; 

  this.authService.signIn(provider).then(user => {
    (action==='IN')?this.signinSocial(user):this.signupSocial(user,type)
  });
  
}




  
}
