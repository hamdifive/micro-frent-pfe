import { Component } from '@angular/core';

@Component({
  selector: 'MS8-empty-route',
  template: '<router-outlet></router-outlet>',
})
export class EmptyRouteComponent {
}
