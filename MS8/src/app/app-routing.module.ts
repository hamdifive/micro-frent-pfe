import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import {RecipientProfilComponent} from'./recipient-profil/recipient-profil.component';


import {RecipientCommandePersoComponent} from'./recipient-commande-perso/recipient-commande-perso.component'
import {RecipientSigninComponent} from'./recipient-signin/recipient-signin.component'




const routes: Routes = [
  { path: 'MS8', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: RecipientCommandePersoComponent
        
    },
    {
      path: 'exemple2',
      component: RecipientProfilComponent
      
  },
  {
    path: 'exemple3',
    component: RecipientSigninComponent
    
}
    
  
  
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
