import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderDeliveryHistoryComponent } from './sender-delivery-history.component';

describe('SenderDeliveryHistoryComponent', () => {
  let component: SenderDeliveryHistoryComponent;
  let fixture: ComponentFixture<SenderDeliveryHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderDeliveryHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderDeliveryHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
