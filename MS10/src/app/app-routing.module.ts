import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './_guards/auth-guard-admin.service';
import { DeliveriesInProgressComponent } from'./deliveries-in-progress/deliveries-in-progress.component';
import { SenderDeliveryHistoryComponent } from'./sender-delivery-history/sender-delivery-history.component';
import { SenderDeliveryWaitListComponent } from'./sender-delivery-wait-list/sender-delivery-wait-list.component';




const routes: Routes = [
  { path: 'MS10', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: DeliveriesInProgressComponent
        
    },
    
    {
      path: 'exemple2',
      component: SenderDeliveryHistoryComponent
      
  },
  {
    path: 'exemple3',
    component: SenderDeliveryWaitListComponent
    
},
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
