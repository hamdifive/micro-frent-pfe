import { Component } from '@angular/core';

@Component({
  selector: 'MS11-empty-route',
  template: '<router-outlet></router-outlet>',
})
export class EmptyRouteComponent {
}
