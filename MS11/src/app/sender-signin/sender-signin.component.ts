import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sender } from 'src/app/_models/sender';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';
import { passwordValidator } from 'src/app/_models/custom-validators';
import { GoogleComponent } from 'src/app/google/google/google.component';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';

@Component({
  selector: 'app-sender-signin',
  templateUrl: './sender-signin.component.html',
  styleUrls: ['./sender-signin.component.scss']
})
export class SenderSigninComponent implements OnInit {
  @ViewChild('frame', { static: true }) public formModal;
  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  @ViewChild(GoogleComponent,{ static: true }) child : GoogleComponent;

  signInForm: FormGroup;
  signUpForm: FormGroup;
  /*
  0 : no error
  1:signin mail or password invalid
  2:signup mail exists in database
  3:signup mail invalid
  4:signin account not verified
  5:signup error
  6:signin success
  */
  message=0;
  new:boolean = false;
  selectedFiles: FileList;
  newSender= new Sender();
  checkbox=false;
  public innerWidth: any;
  constructor(private senderService:SenderService,private loginService:SenderLoginService,private deliveryFeeService:DeliveryFeeService) { }

  ngOnInit() {
    
    this.innerWidth = window.innerWidth;
       this.signInFormBuild();
    this.signUpFormBuild();
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  signInFormBuild(){
    this.signInForm =  new FormGroup({
      email: new FormControl('',[ Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
  }
  signUpFormBuild(){
    this.signUpForm =  new FormGroup({
      name:new FormControl('',Validators.required),
      lastName:new FormControl(''),
      email: new FormControl('', [ Validators.email, Validators.required]),
      address: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required,Validators.minLength(5),passwordValidator()]),
      type:new FormControl(true, Validators.required),
      banner: new FormControl('', [Validators.required]),
    });
    this.newSender.coordinates=null;
  }
  get getSignInFormEmail() {
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }
  get getSignupFormName() {
    return this.signUpForm.get('name');
  }

  get getSignupFormEmail() {
    return this.signUpForm.get('email');
  }

  get getSignupFormPassword() {
    return this.signUpForm.get('password');
  }
  get getSignupFormType() {
    return this.signUpForm.get('type');
  }
  get getSignupFormBanner() {
    return this.signUpForm.get('banner');
  }
  get getSignupFormLastName() {
    return this.signUpForm.get('lastName');
  }
  get getSignupFormAddress() {
    return this.signUpForm.get('address');
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.signUpForm.get('banner').setValue(this.selectedFiles.item(0).name) 
  }
  signin(){
    let sender= new Sender(this.signInForm.value);
    
    this.senderService.getLoginSender(sender.email,sender.password).subscribe(newsender=>{
      if(newsender){
        this.message=0;
        (newsender.verified)?this.loginService.login(newsender._id,true):this.message=4;;
      }else {
        this.message=1;
      }
    })
  }
  check(){
    if(this.getSignupFormEmail.valid){
    this.deliveryFeeService.getOneDeliveryFee().subscribe(data=>{
      let value = data as any
      this.senderService.checkMail(value.key,this.signUpForm.value.email).subscribe( data =>{
        if(data.status===200){
          this.message=0;
        }
        else {
          this.message=3
        }
      })
    })
  }
  }
  signup(){
    this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
      let sender= new Sender(this.signUpForm.value);
       sender.coordinates=this.newSender.coordinates;
     sender.banner=data;
    (sender.type)?sender.type="0":sender.type="1";
    this.senderService.createSender(sender).subscribe(newsender=>{
        this.message=0
        if(newsender===true as any ){
          this.message=2;
        }else if(newsender!=null) {
          this.message=0;
          newsender.coordinates = this.newSender.coordinates
          this.new=false;
          this.message=6;
          this.signUpFormBuild();
          
        
      }
    /*    */
    } ); 
    },err=>{console.log(err);this.message=5;}   );

  }
  setAddress(){
  this.newSender.coordinates ={latitude:this.child.latitude,longitude:this.child.longitude};
  console.log(this.newSender.coordinates )
    this.newSender.address = this.child.address
    this.signUpForm.controls['address'].setValue(this.newSender.address);
  }


}
