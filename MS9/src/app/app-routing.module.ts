import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './_guards/auth-guard-admin.service';

import { SenderUpdateProductComponent } from'./sender-update-product/sender-update-product.component';


import { SenderNewProductComponent } from'./sender-new-product/sender-new-product.component';
import { SenderProfilComponent } from'./sender-profil/sender-profil.component';

import { MailConfirmationComponent } from'./mail-confirmation/mail-confirmation.component';




const routes: Routes = [
  { path: 'MS9', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1/:id',
        component: SenderUpdateProductComponent
        
    },
    

    {
      path: 'exemple2',
      component: SenderNewProductComponent
      
  },
  {
    path: 'exemple3',
    component: SenderProfilComponent
},

{
  path: 'exemple5',
  component: MailConfirmationComponent
  
}
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
