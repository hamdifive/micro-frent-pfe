import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SenderLoginService } from '../_services/sender/sender-login.service';
import { DeliveryManLoginService } from '../_services/deliveryMan/delivery-login.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(
    private _router: Router,private loginService:DeliveryManLoginService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let isLoggedIn = this.loginService.isLoggedIn();
      if (!isLoggedIn) {
        this._router.navigate(['deliveryman']);
        return false;
      } else {
        return true;
      }
  }
}
