import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderProfilComponent } from './sender-profil.component';

describe('SenderProfilComponent', () => {
  let component: SenderProfilComponent;
  let fixture: ComponentFixture<SenderProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
