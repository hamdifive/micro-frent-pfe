import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module
import { HttpClientModule }    from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { GoogleModule} from './/google/google.module';

import { SocialLoginModule, AuthServiceConfig, LoginOpt } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider,AuthService  } from "angularx-social-login";
import { SenderNewProductComponent } from './sender-new-product/sender-new-product.component';
import { SenderUpdateProductComponent } from './sender-update-product/sender-update-product.component';
import { SenderProfilComponent } from './sender-profil/sender-profil.component';
import { MailConfirmationComponent } from './mail-confirmation/mail-confirmation.component';



export function socialConfigs() {    
  const config = new AuthServiceConfig(    
    [    
      {    
        id: FacebookLoginProvider.PROVIDER_ID,    
        provider: new FacebookLoginProvider("440980886545911")   
      },    
      {    
        id: GoogleLoginProvider.PROVIDER_ID,    
        provider: new GoogleLoginProvider("962001351326-h47a80h4bobqgldu55c2n55kusfn1m87.apps.googleusercontent.com")   
      }    
    ]    
  );    
  return config;    
}
@NgModule({
  declarations: [AppComponent,EmptyRouteComponent, SenderNewProductComponent, SenderUpdateProductComponent, SenderProfilComponent, MailConfirmationComponent],
  imports: [
    
    AppRoutingModule,
    BrowserModule,
    ReactiveFormsModule,
    MDBBootstrapModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),

    CommonModule,
    GoogleModule,
    TruncateModule,
    BrowserAnimationsModule,
    MDBBootstrapModule,
    
     
   
  ],
  providers: [ 
    AuthService,    
    {    
      provide: AuthServiceConfig,    
      useFactory: socialConfigs    
    }
   ],

  bootstrap: [AppComponent],
  exports: [ ],
})
export class AppModule { }
