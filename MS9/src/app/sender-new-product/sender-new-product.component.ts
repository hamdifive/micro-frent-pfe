import { Component, OnInit } from '@angular/core';
import { Product, Option, OptionLine } from 'src/app/_models/product';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';
import { Sender } from 'src/app/_models/sender';
import { Router } from '@angular/router';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category/category.service';

@Component({
  selector: 'app-sender-new-product',
  templateUrl: './sender-new-product.component.html',
  styleUrls: ['./sender-new-product.component.scss']
})
export class SenderNewProductComponent implements OnInit {
  selectedProduct:Product = new Product()
  banner:any="";
  selectedFiles: FileList;
  sender:Sender = new Sender();
  categories:Category[]= []
  image=false;
  DaysOfTheWeek = ["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Tous les jours","Non Disponible"];
  constructor(private senderService:SenderService,private loginService:SenderLoginService, private router : Router,private categoryService:CategoryService) { }

  ngOnInit() {
    this.initOption();
    this.getSender();
    this.getCategories();
  }

  getCategories(){
   this.categoryService.getAllcategories().subscribe(categories=>{
     this.categories=categories;
   });
 }
  getSender(){
    this.senderService.getSender(this.loginService.getSender()).subscribe(sender=>{
      this.sender = sender;
    });
  }
 initOption(){
   this.selectedProduct.options[0] = new Option();
   this.initOptionLine(this.selectedProduct.options[0])
 }
 initOptionLine(option:Option){
  option.optionLines[0] = new OptionLine();
}
addOption(){
  let id =this.selectedProduct.options.length;
  this.selectedProduct.options[id] = new Option();
  this.initOptionLine(this.selectedProduct.options[id])
}
addOptionLine(idO){
  let id =this.selectedProduct.options[idO].optionLines.length;
  this.selectedProduct.options[idO].optionLines[id] = new OptionLine();
}
selectFile(event) {
  this.selectedFiles = event.target.files;
  this.banner=this.selectedFiles.item(0).name
  this.image=true;
}
addProduct(){
  if(this.image){
    this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
      this.selectedProduct.image = data 
      let id =(this.sender.menu)?this.sender.menu.length:0;
      this.sender.menu=(this.sender.menu)?this.sender.menu:[];
      this.sender.menu[id] = this.selectedProduct;
      this.update()



    })
  }else {
    let id =(this.sender.menu)?this.sender.menu.length:0;
      this.sender.menu=(this.sender.menu)?this.sender.menu:[];
      this.sender.menu[id] = this.selectedProduct;
      this.update()
  }
 
}
update(){
  this.senderService.updateSender(this.sender).subscribe(sender=>{console.log(sender) ;this.sender = sender
    this.router.navigate(["restaurant/account"])})
  
 }
}
