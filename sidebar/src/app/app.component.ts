import { Component ,OnInit} from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';



@Component({
  selector: 'sidebar-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'sidebar';
  yoshiUrl = assetUrl("yoshi.png");
  
  constructor() { }

  ngOnInit() {
 
}
}