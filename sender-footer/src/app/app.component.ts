import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sender-footer-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
