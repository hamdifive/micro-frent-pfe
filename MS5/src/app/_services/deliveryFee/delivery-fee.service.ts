import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import { DeliveryFee } from 'src/app/_models/deliveryFee';
@Injectable({
  providedIn: 'root'
})
export class DeliveryFeeService {

  env : Env = new Env();
  private Url = this.env.host+'/deliveryfeeservice/';
  constructor(private http: HttpClient) { }
       /** POST: add a new DeliveryFee for the first time */
       createDeliveryFee (deliveryFee: DeliveryFee): Observable<DeliveryFee> {

        return this.http.post<DeliveryFee>(this.Url+'deliveryFee', deliveryFee)
          .pipe(
            catchError(this.handleError<DeliveryFee>('createDeliveryFee', deliveryFee))
          );
      }
        
          /** GET: get one DeliveryFee   */
        getOneDeliveryFee (): Observable<DeliveryFee> {
          return this.http.get<DeliveryFee>(this.Url+'oneDeliveryFee')
            .pipe(
              catchError(this.handleError<DeliveryFee>('oneDeliveryFee', null))
            );
        }
         /** GET: get one DeliveryFee   */
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
