import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './_guards/auth-guard-admin.service';

import {HistoriqueLivraisonComponent} from'./historique-livraison/historique-livraison.component'
import {DeliverylistComponent} from'./deliverylist/deliverylist.component'
import {DeliveryInprogressComponent} from'./delivery-inprogress/delivery-inprogress.component'




import {AffectedDeliveryListComponent} from'./affected-delivery-list/affected-delivery-list.component'


const routes: Routes = [
  { path: 'MS5', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: HistoriqueLivraisonComponent
        
    },
    {
      path: 'exemple2',
      component: DeliverylistComponent
      
  },
  {
    path: 'exemple3',
    component: DeliveryInprogressComponent
    
}
  
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
