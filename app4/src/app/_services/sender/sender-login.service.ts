import { Injectable } from '@angular/core';
import SimpleCrypto from "simple-crypto-js";
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SenderLoginService {
  _secretKey = "Five12345";
  simpleCrypto = new SimpleCrypto(this._secretKey);
  constructor(private router : Router) { }
  encrypt(object){

    return  this.simpleCrypto.encrypt(object);
  }
  decrypt(object):any{
    return this.simpleCrypto.decrypt(object); 
  }
 

 
  isLoggedIn():Boolean{

        return Boolean(JSON.parse(sessionStorage.getItem('isLoggedInS'))); 

  }
 
  logoutAdmin() {

    sessionStorage.removeItem('isLoggedInS');
    sessionStorage.removeItem('sender');

  this.router.navigate(["admin"]);
}
}
