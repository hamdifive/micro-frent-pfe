import { Component ,OnInit} from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';



@Component({
  selector: 'app4-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app4';
  yoshiUrl = assetUrl("yoshi.png");
  
  constructor(private loginService:SenderLoginService) { }

  ngOnInit() {
  }
 logout(){
   this.loginService.logoutAdmin()
 }
}
