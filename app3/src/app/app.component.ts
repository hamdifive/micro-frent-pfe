import { Component ,OnInit} from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';



@Component({
  selector: 'app3-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app3';
  yoshiUrl = assetUrl("yoshi.png");
  
  ngOnInit() {
  
  }
}