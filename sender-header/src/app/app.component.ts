import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sender } from 'src/_models/sender';
import { SenderLoginService } from 'src/_services/sender/sender-login.service';
import { Router } from '@angular/router';
@Component({
  selector: 'sender-header-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  constructor(public router: Router,private loginService:SenderLoginService) { }

  ngOnInit() {
   
  }
  logout(){
    this.loginService.logout();
  }

}
