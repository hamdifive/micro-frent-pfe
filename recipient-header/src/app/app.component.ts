import { Component, OnInit, ViewChildren, QueryList, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Recipient } from 'src/_models/recipient';
import { RecipientLoginService } from 'src/_services/recipient/recipient-login.service';
import { Router } from '@angular/router';
import { GoogleComponent } from 'src/google/google/google.component';
import { DataService } from 'src/_services/exchange/data.service';
import { GoogleService } from 'src/_services/google.service';

@Component({
  selector: 'recipient-header-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit,AfterViewInit {

  @ViewChildren(GoogleComponent) child : QueryList<GoogleComponent>;
  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  searchValue;
  message:string;

  constructor(private googleService:GoogleService,private data: DataService,public router: Router,public loginService:RecipientLoginService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)
    

  }
  ngAfterViewInit(): void {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.data.changeMessage({
          latitude:position.coords.latitude,
          longitude:position.coords.longitude
        })
      
      },(error)=> {
        this.googleService.getLocation().subscribe(coords=>{
          this.data.changeMessage({
            latitude:coords.location.lat,
            longitude:coords.location.lng
          })
        
        
        }) 
      });
    }
 
    
  }

  logout(){
    this.loginService.logout();
  }
  getLocation(){
    this.searchValue = this.child.first.address;
  }
  validate(){
    this.data.changeMessage({
      latitude:this.child.first.latitude,
      longitude:this.child.first.longitude
    })

  }
}
