import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import {DeliveryFeeComponent} from'./delivery-fee/delivery-fee.component'



const routes: Routes = [
  { path: 'MS4', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: DeliveryFeeComponent
        
    },
    
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
