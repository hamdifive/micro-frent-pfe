import { Component, OnInit } from '@angular/core';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { Sender } from 'src/app/_models/sender';

@Component({
  selector: 'recipient-footer-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  senders:Sender[]=[];
  constructor(private senderService:SenderService) { }

  ngOnInit() {
  this.getSenders()
  }
  getSenders(){
    this.senderService.getAllSenders().subscribe(senders=>{
      this.senders=senders.slice(0,3);
    }) }

}
