export class DeliveryFee {

    _id:string;
    feeDT:number;
    priceKm:number;
    orderLimit:number;
    limit:number;
    profitPercentage:number;
    feePercentage:number;
    feePercentageOverLimit:number;
    key:string


}