import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './_guards/auth-guard-admin.service';
import {NotesComponent} from'./notes/notes.component'

import {RecipientHistoriqueComponent} from'./recipient-historique/recipient-historique.component'
import {RecipientListRestoComponent} from'./recipient-list-resto/recipient-list-resto.component'
import { RecipientSuiviComComponent} from'./recipient-suivi-com/recipient-suivi-com.component'







const routes: Routes = [
  { path: 'MS7', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: RecipientHistoriqueComponent
        
    },
    {
      path: 'exemple2',
      component: RecipientSuiviComComponent
      
  },
    
    {
      path: 'exemple3',
      component: RecipientListRestoComponent
      
  },
  {
    path: 'exemple4',
    component: NotesComponent
    
},
   
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
