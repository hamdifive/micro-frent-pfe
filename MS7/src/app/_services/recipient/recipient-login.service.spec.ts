import { TestBed } from '@angular/core/testing';

import { RecipientLoginService } from './recipient-login.service';

describe('RecipientLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecipientLoginService = TestBed.get(RecipientLoginService);
    expect(service).toBeTruthy();
  });
});
