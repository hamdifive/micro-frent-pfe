import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Sender } from 'src/app/_models/sender';
import { DeliveryFeeService } from '../deliveryFee/delivery-fee.service';

@Injectable({
  providedIn: 'root'
})
export class SenderService {

  env : Env = new Env();
  private Url = this.env.host+'/senderservice/';
  path=location.origin+"/restaurant/confirmation"
  constructor(private http: HttpClient) { }
     /** GET: checkMail */
     checkMail(key,mail): Observable<any> {
      
         return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
         .pipe(
           catchError(err => {
             console.log('caught mapping error and rethrowing', err);
             return throwError(err );
         })
         )
      
   
    }
    
  
       /** GET: get one Sender   */
  getSender (id: string): Observable<Sender> {
    return this.http.get<Sender>(this.Url+'sender/'+id)
      .pipe(
        catchError(this.handleError<Sender>('getSender', null))
      );
  }
        /** GET: get all Senders   */
  getAllSenders (): Observable<Sender[]> {
    return this.http.get<Sender[]>(this.Url+'sender')
      .pipe(
        catchError(this.handleError<Sender[]>('getAllSenders', []))
      );
  }
  /** GET: Login  */
 getLoginSender (mail,password): Observable<Sender> {
 return this.http.get<Sender>(this.Url+'login/'+mail+'/'+password)
  .pipe(
   catchError(this.handleError<Sender>('getLoginSender', null))
    );
  }
       /** DELETE: delete one Sender   */
 
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
