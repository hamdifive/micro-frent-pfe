import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Delivery } from 'src/app/_models/delivery';
import { SenderLoginService } from '../sender/sender-login.service';
import { RecipientLoginService } from '../recipient/recipient-login.service';
import { DeliveryManLoginService } from '../deliveryMan/delivery-login.service';
import {Notification} from './../../_models/notification';


@Injectable({
  providedIn: 'root'
})
export class DeliveryService {
  env : Env = new Env();
  private Url = this.env.host+'/deliveryservice/';
  constructor(private http: HttpClient,private loginService:SenderLoginService,private recipientLoginService:RecipientLoginService) { }

        




          /** GET: Retrieve all deliveries belonging to a Recipient with specific status*/
          getDeliveryByRecipientAndStatus (status): Observable<Delivery[]> {
            //let  idRecipient= this.recipientLoginService.getRecipient();
             let idRecipient ="5db99cf0b4606a3c1cb0e4ac";
           // let  idRecipient= this.recipientLoginService.getRecipient();
             return this.http.get<Delivery[]>(this.Url+'deliveryByStatusAndRecipient/'+idRecipient+'/'+status)
              .pipe(
               catchError(this.handleError<Delivery[]>('getDeliveryByRecipientAndStatus', null))
                );
              }

        
          private handleError<T> (operation = 'operation', result?: T) {
            return (error: any): Observable<T> => {
         
              // TODO: send the error to remote logging infrastructure
              console.error(error); // log to console instead
         
              // TODO: better job of transforming error for user consumption
              console.log(`${operation} failed: ${error.message}`);
         
              // Let the app keep running by returning an empty result.
              return of(result as T);
            };
          }
}
