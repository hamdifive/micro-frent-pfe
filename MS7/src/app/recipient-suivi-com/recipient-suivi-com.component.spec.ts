import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientSuiviComComponent } from './recipient-suivi-com.component';

describe('RecipientSuiviComComponent', () => {
  let component: RecipientSuiviComComponent;
  let fixture: ComponentFixture<RecipientSuiviComComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientSuiviComComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientSuiviComComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
