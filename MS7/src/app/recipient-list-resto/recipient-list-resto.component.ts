import { Component, OnInit } from '@angular/core';
import{ Sender, Times } from '../_models/sender';
import { SenderService } from '../_services/sender/sender.service';
import { ActivatedRoute } from '@angular/router';
import { readmore } from './../_services/truncate'
import { DataService } from 'src/app/_services/exchange/data.service';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { NoteService } from 'src/app/_services/note/NoteService';

@Component({
  selector: 'app-recipient-list-resto',
  templateUrl: './recipient-list-resto.component.html',
  styleUrls: ['./recipient-list-resto.component.scss']
})
export class RecipientListRestoComponent implements OnInit {
  senders: Sender[];
  allSenders:Sender[]
  title="Nos Recommendations"
  change = readmore;
  p=1;
  coords:any;
  limit=5;
  today= new Date();
  average=[];
  constructor(private data: DataService,private senderService:SenderService, private route:ActivatedRoute,
    private deliveryFeeService:DeliveryFeeService,private noteService:NoteService) { }

  ngOnInit() {
    this.getDeliveryFee();
   
  }

  setStatus(tab){
  if(tab.length===0){return false;}
    let hours =new Date().getHours()  ;
    let minutes =new Date().getMinutes();
    let scheduele:Times = tab[this.today.getDay()]
    
if (scheduele.closed){return true;}
else {
  if(!scheduele.openningTime ||scheduele.openningTime=="00:00" ){return false}

let now = new Date(); now.setHours(hours,minutes,0)
 let opH =+ scheduele.openningTime.substring(0,2);
let opM =+ scheduele.openningTime.substring(3,5);
let open = new Date(); open.setHours(opH,opM,0)
let closeH =+ scheduele.closingTime.substring(0,2);
let closeM =+ scheduele.closingTime.substring(3,5);
let close = new Date(); close.setHours(closeH,closeM,0)
if(now > open && now < close){
    return false;
}
else { return true}
}
    }

 getDeliveryFee(){
   this.deliveryFeeService.getOneDeliveryFee().subscribe(value=>{
     this.limit=value.limit
     this.getAllSenders()
   })
 }

  getAllSenders() : void{
    this.title="Nos Recommendations"
    this.senderService.getAllSenders()
    .subscribe(senders => {this.senders = senders;
      this.senders= this.senders.filter(item=> item.type==="0");
      this.senders= this.senders.filter(item=>  item.verified===true);
      this.senders= this.senders.filter(item=>  item.active===true)
      this.allSenders=this.senders
      this.data.currentMessage.subscribe(coords =>{ this.coords =coords
        if(coords){
          console.log('value')
          this.title = "Les restaurants les plus proche de vous"
          let listSenders =this.allSenders.filter(item=> item.coordinates !== null) ;
           listSenders =  listSenders.filter(item=> 
          this.distance(item.coordinates.latitude,item.coordinates.longitude,coords.latitude,coords.longitude) < this.limit  )
          this.senders = listSenders;
          this.senders.forEach(item=>{
            this.noteService.getNotesAverageBySender(item._id).subscribe(value=>{
              this.average.push(value)
            })
          })
          if(listSenders.length === 0){
            this.title = "Vous n'avez pas des restaurants proche de vous !!"

          }
          
        } 
      
      })
    }) 
  }
   distance(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    }
    else {
      var radlat1 = Math.PI * lat1/180;
      var radlat2 = Math.PI * lat2/180;
      var theta = lon1-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      dist = dist * 1.609344 
      return dist;
    }
  }

}
