import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientListRestoComponent } from './recipient-list-resto.component';

describe('RecipientListRestoComponent', () => {
  let component: RecipientListRestoComponent;
  let fixture: ComponentFixture<RecipientListRestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientListRestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientListRestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
