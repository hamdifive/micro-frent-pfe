import { Component } from '@angular/core';

@Component({
  selector: 'MS5-empty-route',
  template: '<router-outlet></router-outlet>',
})
export class EmptyRouteComponent {
}
