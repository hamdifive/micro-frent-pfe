import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module
import { HttpClientModule }    from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { GoogleModule} from './/google/google.module';
import {NgxPaginationModule} from 'ngx-pagination';


import { SocialLoginModule, AuthServiceConfig, LoginOpt } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider,AuthService  } from "angularx-social-login";

import { RecipientHistoriqueComponent } from './recipient-historique/recipient-historique.component';
import { RecipientListRestoComponent } from './recipient-list-resto/recipient-list-resto.component';
import { RecipientMenuRestoComponent } from './recipient-menu-resto/recipient-menu-resto.component';
import { NotesComponent } from './notes/notes.component';
import { RecipientSuiviComComponent } from './recipient-suivi-com/recipient-suivi-com.component';



export function socialConfigs() {    
  const config = new AuthServiceConfig(    
    [    
      {    
        id: FacebookLoginProvider.PROVIDER_ID,    
        provider: new FacebookLoginProvider("440980886545911")   
      },    
      {    
        id: GoogleLoginProvider.PROVIDER_ID,    
        provider: new GoogleLoginProvider("962001351326-h47a80h4bobqgldu55c2n55kusfn1m87.apps.googleusercontent.com")   
      }    
    ]    
  );    
  return config;    
}
@NgModule({
  declarations: [AppComponent,EmptyRouteComponent, RecipientHistoriqueComponent, RecipientListRestoComponent, RecipientMenuRestoComponent, NotesComponent, RecipientSuiviComComponent],
  imports: [
    
    AppRoutingModule,
    BrowserModule,
    ReactiveFormsModule,
    MDBBootstrapModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    NgxPaginationModule,
    CommonModule,
    GoogleModule,
    TruncateModule,
    BrowserAnimationsModule,
    MDBBootstrapModule,
    
     
   
  ],
  providers: [ 
    AuthService,    
    {    
      provide: AuthServiceConfig,    
      useFactory: socialConfigs    
    }
   ],

  bootstrap: [AppComponent],
  exports: [ ],
})
export class AppModule { }
