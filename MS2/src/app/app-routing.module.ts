import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import {AjoutRestaurantComponent} from './Ajout-restaurant/Ajout-restaurant.component';
import {CategoryListComponent} from './category-list/category-list.component';
import { AuthGuardService } from './_guards/auth-guard-admin.service';
import {PageRestaurantsComponent} from './page-restaurants/page-restaurants.component';


const routes: Routes = [
  { path: 'MS2', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: AjoutRestaurantComponent
        
    },
    {
      path: 'exemple2',
      component: CategoryListComponent
  },
  {
    path: 'exemple3/:id',
    component: PageRestaurantsComponent
}]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
