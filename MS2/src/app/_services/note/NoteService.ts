import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Env } from '../env';
import { Note } from '../../_models/note';
import { RecipientLoginService } from '../recipient/recipient-login.service';
import { DeliveryManLoginService } from '../deliveryMan/delivery-login.service';
@Injectable({
  providedIn: 'root'
})
export class NoteService {
  env: Env = new Env();
  private Url = this.env.host + '/noteservice/';
  constructor(private http: HttpClient, private recipientLoginService: RecipientLoginService,private deliverymanLoginService:DeliveryManLoginService) { }
  /** POST: add a new Note  */
 
    /** GET: get all Notes  belonnig to a Sender */
    getNotesBySender(id): Observable<Note[]> {
      return this.http.get<Note[]>(this.Url + 'noteBySender/' + id)
        .pipe(catchError(this.handleError<Note[]>('getNotesBySender', [])));
    }
    /** GET: get  Note's average  belonnig to a Sender */
    getNotesAverageBySender(id?): Observable<number> {
      return this.http.get<number>(this.Url + 'AverageBySender/' + id)
        .pipe(catchError(this.handleError<number>('getNotesAverageBySender', null)));
    }
 
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
