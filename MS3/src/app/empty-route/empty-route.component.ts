import { Component } from '@angular/core';

@Component({
  selector: 'MS2-empty-route',
  template: '<router-outlet></router-outlet>',
})
export class EmptyRouteComponent {
}
