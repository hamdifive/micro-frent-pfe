import { NgModule } from '@angular/core';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { APP_BASE_HREF } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './_guards/auth-guard-admin.service';
import {FoodDeliveriesComponent} from'./food-deliveries/food-deliveries.component'
import {ListRestaurantsComponent} from'./list-restaurants/list-restaurants.component'
import {RecipientListComponent} from'./recipient-list/recipient-list.component'
import {UpdateRestaurantComponent} from'./update-restaurant/update-restaurant.component'

UpdateRestaurantComponent

const routes: Routes = [
  { path: 'MS3', component: EmptyRouteComponent,
  
     children: [
      {
        path: 'exemple1',
        component: FoodDeliveriesComponent
        
    },
    {
      path: 'exemple2',
      component: ListRestaurantsComponent
      
  },
  {
    path: 'exemple3/:id',
    component: UpdateRestaurantComponent
    
},
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppRoutingModule { }
