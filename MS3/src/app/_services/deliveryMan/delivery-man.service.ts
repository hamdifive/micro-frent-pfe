import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  DeliveryMan } from 'src/app/_models/deliveryMan';
@Injectable({
  providedIn: 'root'
})
export class DeliveryManService {

  env : Env = new Env();
  path=location.origin+"/deliveryman/confirmation"
  private Url = this.env.host+'/deliverymanservice/';
  constructor(private http: HttpClient) { }
       /** GET: checkMail */
       checkMail(key,mail): Observable<any> {
      
        return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
        .pipe(
          catchError(err => {
            console.log('caught mapping error and rethrowing', err);
            return throwError(err );
        })
        )
     
  
   }
   
  
    
    
     
     
            
              /** GET: get all DeliveryMans   */
        getAllDeliveryMans (): Observable<DeliveryMan[]> {
          return this.http.get<DeliveryMan[]>(this.Url+'DeliveryMan')
            .pipe(
              catchError(this.handleError<DeliveryMan[]>('getAllDeliveryMans', []))
            );
        }

        private handleError<T> (operation = 'operation', result?: T) {
          return (error: any): Observable<T> => {
       
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
       
            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);
       
            // Let the app keep running by returning an empty result.
            return of(result as T);
          };
        }
}
