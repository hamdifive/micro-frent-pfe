import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Recipient } from 'src/app/_models/recipient';

@Injectable({
  providedIn: 'root'
})
export class RecipientService {

  env : Env = new Env();
  private Url = this.env.host+'/recipientservice/';
  path=location.origin+"/confirmation"
  constructor(private http: HttpClient) { }
         /** GET: checkMail */
         checkMail(key,mail): Observable<any> {
      
          return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
          .pipe(
            catchError(err => {
              console.log('caught mapping error and rethrowing', err);
              return throwError(err );
          })
          )
       
    
     }
  
      
           /** GET: get one Recipient   */
      getRecipient (id: string): Observable<Recipient> {
        return this.http.get<Recipient>(this.Url+'recipient/'+id)
          .pipe(
            catchError(this.handleError<Recipient>('getRecipient', null))
          );
      }
            /** GET: get all Recipients   */
      getAllRecipients (): Observable<Recipient[]> {
        return this.http.get<Recipient[]>(this.Url+'recipient')
          .pipe(
            catchError(this.handleError<Recipient[]>('getAllRecipients', []))
          );
      }
    
      private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
     
          // TODO: send the error to remote logging infrastructure
          console.error(error); // log to console instead
     
          // TODO: better job of transforming error for user consumption
          console.log(`${operation} failed: ${error.message}`);
     
          // Let the app keep running by returning an empty result.
          return of(result as T);
        };
      }

}
  