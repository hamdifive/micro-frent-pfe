import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module
import { HttpClientModule }    from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FoodDeliveriesComponent } from './food-deliveries/food-deliveries.component';
import { ListRestaurantsComponent } from './list-restaurants/list-restaurants.component';
import { RecipientListComponent } from './recipient-list/recipient-list.component';
import { UpdateRestaurantComponent } from './update-restaurant/update-restaurant.component';


@NgModule({
  declarations: [
    AppComponent,
    EmptyRouteComponent,
    
    
    
    FoodDeliveriesComponent,
    
    
    
    ListRestaurantsComponent,
    
    
    
    RecipientListComponent,
    
    
    
    UpdateRestaurantComponent,
    


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    ReactiveFormsModule,
    MDBBootstrapModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }