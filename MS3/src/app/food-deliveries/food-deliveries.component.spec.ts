import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodDeliveriesComponent } from './food-deliveries.component';

describe('FoodDeliveriesComponent', () => {
  let component: FoodDeliveriesComponent;
  let fixture: ComponentFixture<FoodDeliveriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodDeliveriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodDeliveriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
