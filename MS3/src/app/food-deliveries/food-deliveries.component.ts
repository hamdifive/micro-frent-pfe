import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
@Component({
  selector: 'app-food-deliveries',
  templateUrl: './food-deliveries.component.html',
  styleUrls: ['./food-deliveries.component.css']
})
export class FoodDeliveriesComponent implements OnInit {
  @ViewChild('frame', { static: true }) public formModal;
  
  deliveries:Delivery[]=[];
  selectedId:string="";
  deliveryMen:DeliveryMan[]=[]
  valid=false;

  constructor(private deliveryService:DeliveryService,private orderService:OrderService,private recipientService:RecipientService,
    private delieveryManService:DeliveryManService) { }

  ngOnInit() {
    this.getDeliveryList();
    this.getDeliveryMen();
  }
  getDeliveryList(){
    this.deliveryService.getAllDeliverys().subscribe(deliveries => {
      
      this.deliveries=deliveries;
      this.deliveries.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index)})
    })
  }
 change(delivery:Delivery){
   let profit = delivery.total - (delivery.price+delivery.deliveryFee)
   profit = +profit.toFixed(1);
   return profit
 }
 status(value){
   switch(value){
     case 'init':return "En attente de restaurant";
     case 'wait':return "En attente de livreur";
     case 'accepted':return "En cours";
     case 'delivered':return "Livree"
   }
 }
  getDeliveryMen(){
    this.delieveryManService.getAllDeliveryMans().subscribe(deliveryMen=>{
      this.deliveryMen=deliveryMen;
    })
  }

  

  getOrder(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this.deliveries[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
        });
      }  
    }
    affect(id){
      this.deliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
       
        delivery.deliveryMan=id;
        delivery.status="affected";
        this.deliveryService.updateDelivery(delivery).subscribe(delivery=>{
          this.formModal.hide();this.getDeliveryList();
        })
      })
    }


}

