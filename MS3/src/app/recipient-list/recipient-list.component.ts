import { Component, OnInit } from '@angular/core';
import { Recipient } from 'src/app/_models/recipient';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';

@Component({
  selector: 'app-recipient-list',
  templateUrl: './recipient-list.component.html',
  styleUrls: ['./recipient-list.component.css']
})
export class RecipientListComponent implements OnInit {
  recipients:Recipient[]=[]
  constructor(private recipientService:RecipientService) { }

  ngOnInit() {
    this.getRecipients();
  }
  getRecipients(){
    this.recipientService.getAllRecipients().subscribe(data=>{
      this.recipients=data
      console.log(data)
    })
  }

}
