import { Component, OnInit } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import {SenderService} from 'src/app/_services/sender/sender.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { Sender } from 'src/app/_models/sender';

@Component({
  selector: 'app1-list-restaurants',
  templateUrl: './list-restaurants.component.html',
  styleUrls: ['./list-restaurants.component.css']
})
export class ListRestaurantsComponent implements OnInit {
  senders:Sender[]=[];
  deliveries:Delivery[]=[];
  deliveryMen:DeliveryMan[]=[]
  constructor(private deliveryService:DeliveryService,
    private orderService:OrderService,private recipientService:RecipientService,private SenderService:SenderService,
  ) { }

  ngOnInit() {
    this.getRestList();
  }
  getRestList(){
    this.SenderService.getAllSenders().subscribe(senders => {
      
      this.senders=senders.filter(item => item.type=="0");

    })
  }
  lockSender(sender:Sender,value){
    sender.closed=value;
    this.SenderService.updateSender(sender).subscribe(updatedSender=>{console.log(updatedSender)})
  }
  activeSender(sender:Sender,value){
    sender.active=value;
    this.SenderService.updateSender(sender).subscribe(updatedSender=>{console.log(updatedSender)})
  }
}
